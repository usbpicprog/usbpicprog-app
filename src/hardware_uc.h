/***************************************************************************
*   Copyright (C) 2008 by Frans Schreuder                                 *
*   usbpicprog.sourceforge.net                                            *
*                                                                         *
*   This program is free software; you can redistribute it and/or modify  *
*   it under the terms of the GNU General Public License as published by  *
*   the Free Software Foundation; either version 2 of the License, or     *
*   (at your option) any later version.                                   *
*                                                                         *
*   This program is distributed in the hope that it will be useful,       *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
*   GNU General Public License for more details.                          *
*                                                                         *
*   You should have received a copy of the GNU General Public License     *
*   along with this program; if not, write to the                         *
*   Free Software Foundation, Inc.,                                       *
*   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
***************************************************************************/

#ifndef HARDWAREUC_H
#define HARDWAREUC_H

// IMPORTANT: do not include here <usb.h>; it will give problems on
//            Windows when building with Unicode mode enabled

#include "hexfile.h"
#include "pictype.h"
#include "hardware.h"

#define UPP_VENDOR 0x04D8
#define UPP_PRODUCT 0x000E

#define BOOTLOADER_VENDOR 0x04D8
#define BOOTLOADER_PRODUCT 0x000B

//! The index of the endpoint used for communication.
#define ENDPOINT_INDEX 1

//! The endpoint used for reading data from UPP bootloader/programmer.
#define READ_ENDPOINT (ENDPOINT_INDEX | LIBUSB_ENDPOINT_IN)

//! The endpoint used for writing data to the UPP bootloader/programmer.
#define WRITE_ENDPOINT (ENDPOINT_INDEX | LIBUSB_ENDPOINT_OUT)

//! Timeout in milliseconds for USB operations.
#define USB_OPERATION_TIMEOUT 3000

//! Return code for the HardwareUC class' functions when the operations have been
//aborted.
#define OPERATION_ABORTED 2

// forward declaration:
class UppMainWindow;
struct libusb_device_handle;

/**
    This class connects to the USB PIC programmer hardware using libusb.

    Libusb (http://libusb.wiki.sourceforge.net/) allows user level applications
    to access USB devices regardless of OS.
    API docs for that library is at http://libusb.sourceforge.net/doc/

    This class opens two hardware endpoints for reading/writing to the connected
    hardware respectively identified by the @c READ_ENDPOINT and @c
   WRITE_ENDPOINT
    constants.

    TODO: add const attributes to func arguments not modified
*/
class HardwareUC:public Hardware {
  public:
    /**
        Default constructor.
        Before using an HardwareUC instance you'll need to call connect()
       yourself.
    */
    HardwareUC();

    /**
        Calls disconnect().
    */
    ~HardwareUC();

    /**
        Attempts to connect to the specified hardware type.

        @param CB
            Links this instance with the parent UppMainWindow window; this link
       is used
            for updating the progress bar.
            If NULL is given, progress bar is not updated.
        @param hwtype
            Indicates the hardware type which should be searched first.
            If no hardware of the given type is found, then the other types of
       supported
            hardwares are searched.
    */

    void setConnParams(HardwareType hwtype);
    bool connect(UppMainWindow *CB = NULL);

    /**
        Disconnects from the hardware currently attached, if there's any.
    */
    bool disconnect();

    /**
        The functions of this group returns the number of bytes successfully
       read/write
        or -1 if there was an error.
    */
    //@{

    /** Read a string of data from the connected hardware (through
     * usb_interrupt_read). */
    int readString(unsigned char *msg, int size, bool noerror = false) const;

    /** Send a string of data to the connected hardware (through usb_interrupt
     * write). */
    int writeString(const unsigned char *msg, int size, bool noerror = false) const;

    /** If the OS supports it, tries to detach an already existing driver */
    void tryToDetachDriver();

   /** Check if some supported hardware is successfully connected to */
    bool connected() const
    {
        return m_handle != NULL;
    }

  private: // libusb-related members
           /** Device handle containing information about Usbpicprog when it's
            * connected */
    libusb_device_handle *m_handle;

    /**
        The mode (interrupt or bulk) of the endpoint used for UPP->SW
       communications.
        Note that this variable should be of type "enum libusb_transfer_type"
       but
        hardware.h doesn't include libusb.h directly so that the definition of
       that
        enum is not available to the compiler here. Thus a generic int is used
       instead.
    */
    int m_modeReadEndpoint;

    /**
        The mode (interrupt or bulk) of the endpoint used for SW->UPP
       communications.
        Note that this variable should be of type "enum libusb_transfer_type"
       but
        hardware.h doesn't include libusb.h directly so that the definition of
       that
        enum is not available to the compiler here. Thus a generic int is used
       instead.
    */
    int m_modeWriteEndpoint;
};

#endif // HARDWAREUC_H
