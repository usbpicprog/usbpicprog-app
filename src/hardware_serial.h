/***************************************************************************
*   This program is free software; you can redistribute it and/or modify  *
*   it under the terms of the GNU General Public License as published by  *
*   the Free Software Foundation; either version 2 of the License, or     *
*   (at your option) any later version.                                   *
*                                                                         *
*   This program is distributed in the hope that it will be useful,       *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
*   GNU General Public License for more details.                          *
*                                                                         *
*   You should have received a copy of the GNU General Public License     *
*   along with this program; if not, write to the                         *
*   Free Software Foundation, Inc.,                                       *
*   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
***************************************************************************/

#ifndef HARDWARESERIAL_H
#define HARDWARESERIAL_H

// IMPORTANT: do not include here <usb.h>; it will give problems on
//            Windows when building with Unicode mode enabled
#include <termios.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdint.h>


#include "hexfile.h"
#include "pictype.h"
#include "hardware.h"

// forward declaration:
class UppMainWindow;

#define MAKE_UL(_a,_b,_c,_d)    ((uint32_t)(_a) << 24 | \
                                 (uint32_t)(_b)<<16 | \
                                 (uint32_t)(_c)<<8 | (_d))

#define RESP_MAGIC              MAKE_UL('U', 'S', 'B', 'P')
#define MAX_RESP_SIZE           48

#define min(a,b)    (a) < (b) ?(a):(b)

#define DEFAULT_SERIAL_DEVICE   "/dev/ttyACM0"

struct response_header {
    uint32_t magic;
    uint16_t size;
}__attribute__((packed));
/**
    This class connects to the PIC programmer hardware using serial.
*/
class HardwareSerial:public Hardware {
  public:
    /**
        Default constructor.
        Before using an HardwareSerial instance you'll need to call connect()
       yourself.
    */
    HardwareSerial();

    /**
        Calls disconnect().
    */
    ~HardwareSerial();

    /**
        Attempts to connect to the specified hardware type.

        @param CB
            Links this instance with the parent UppMainWindow window; this link
       is used
            for updating the progress bar.
            If NULL is given, progress bar is not updated.
        @param hwtype
            Indicates the hardware type which should be searched first.
            If no hardware of the given type is found, then the other types of
       supported
            hardwares are searched.
    */
    void setConnParams(const char *deviceName);
    bool connect(UppMainWindow *CB = NULL);

    /**
        Disconnects from the hardware currently attached, if there's any.
    */
    bool disconnect();

    /**
        The functions of this group returns the number of bytes successfully
       read/write
        or -1 if there was an error.
    */
    //@{

    /** Read a string of data from the connected hardware (through
     * usb_interrupt_read). */
    int readString(unsigned char *msg, int size, bool noerror = false) const;

    /** Send a string of data to the connected hardware (through usb_interrupt
     * write). */
    int writeString(const unsigned char *msg, int size, bool noerror = false) const;

    /** Check if some supported hardware is successfully connected to */
    bool connected() const
    {
        return m_handle > 0;
    }

    private:
        int m_handle;
        int m_baudex;
        struct termios m_oldtio;
        const char *m_deviceName;
        // private helpers
        int readBytes(uint8_t *msg, int size) const;
        bool targetDeviceGood();
        void resetTarget(uint8_t mode);
};


#endif // HARDWARESERIAL_H
