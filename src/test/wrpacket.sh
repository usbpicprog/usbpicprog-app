#!/bin/bash
file=$1
i=0
for ch in `cat $file`; do
    # skip the offset line or comment
    if [[ $ch == *: ]] || [[ $ch == \#* ]]; then
        continue
    fi
    echo -ne "\x$ch";
    i=$((i+1))
done

# echo ""
# echo "Wrote $i bytes"
