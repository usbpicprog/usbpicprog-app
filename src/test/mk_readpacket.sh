#!/bin/bash

prhex()
{
    for val in $*; do
        printf "%x " "$val"
    done
}

ch=$1
sz=$2
shift 2
off=${*:-"0 0 0"}
cmd=$((0x30))

if [[ -z $ch ]] || [[ -z $sz ]];then
    echo "$0 val size [off]"
    exit 1
fi

type=1

prhex "$cmd $sz "
echo -n "$off $type "
for ((i=0; i<$sz; i++)); do
    prhex "$((ch+i)) "
done
